<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<title>КАМКОР Лечение глистных инвазий в Шымкенте</title>
	<link rel="stylesheet" href="style.css">
	<link rel="shortcut icon" href="favicon.png" type="image/png">
	<link rel="preload" href="style.css" as="style">
	<link rel="preload" href="fonts/opensans-bold.woff" as="font" crossorigin>
	<link rel="preload" href="fonts/opensans-regular.woff" as="font" crossorigin>
</head>
<body>
	<header class="header">
		<div class="header__inner">
			<img src="imgs/logo.svg" alt="Kamkor medical center" class="header__logo">
			<nav class="header__nav">
				<a href="#programm" class="header__nav-a header__nav-a_programm">ПРОГРАММА ОЧИЩЕНИЯ</a>
				<a href="#about" class="header__nav-a header__nav-a_about">О НАС</a>
				<a href="#contacts" class="header__nav-a header__nav-a_contacts">КОНТАКТЫ</a>
			</nav>
		</div>
	</header>
	<div class="hero">
		<div class="hero__items">
			<div class="hero__empty"></div>
			<div class="hero__trash">
				<div class="hero__trash-text">
					ВЫСОКОЭФФЕКТИВНОЕ
					<br>
					<b>ОЧИЩЕНИЕ ОРГАНИЗМА</b>
					<br>
					ОТ ГЕЛЬМИНТОВ
				</div>
				<a href="tel:+77252771032" class="hero__trash-phone">+7 (7252) <b>77 10 32</b></a>
			</div>
		</div>
		<div class="hero__text">
			<div>Если у Вас или у Вашего ребёнка наблюдается более <b>5 симптомов</b> из списка,</div>
			<div> есть все основания подозревать <b>глистную инвазию.</b></div>
		</div>
	</div>
	<div class="trash-mobile">
		<div class="hero__trash">
			<div class="hero__trash-text">
				ВЫСОКОЭФФЕКТИВНОЕ
				<br>
				<b>ОЧИЩЕНИЕ ОРГАНИЗМА</b>
				<br>
				ОТ ГЕЛЬМИНТОВ
			</div>
			<a href="tel:+77252771032" class="hero__trash-phone">+7 (7252) <b>77 10 32</b></a>
		</div>
		<div class="trash-mobile__text">
			<div>Если у Вас или у Вашего ребёнка наблюдается более <b>5 симптомов</b> из списка, есть все основания подозревать <b>глистную инвазию.</b></div>
		</div>
	</div>
	<section class="list">
		<ul class="list__inner">
			<li class="list__item">Ночной скрежет зубами</li>
			<li class="list__item">Запоры или понос</li>
			<li class="list__item">Вздутие живота</li>
			<li class="list__item">Аллергия</li>
			<li class="list__item">Пятна и высыпания на коже</li>
			<li class="list__item">Зуд в области ануса</li>
			<li class="list__item">Пристрастие к сладкому</li>
			<li class="list__item">Аллергический кашель</li>
			<li class="list__item">Расслоение ногтей</li>
			<li class="list__item">Выпадение волос</li>
			<li class="list__item">Снижение аппетита</li>
			<li class="list__item">Потеря веса</li>
			<li class="list__item">Плохой сон</li>
			<li class="list__item">Депрессии</li>
			<li class="list__item">Чувство усталости</li>
			<li class="list__item">Потливость</li>
		</ul>
	</section>
	<section class="programm" id="programm">
		<div class="programm__header">ПРОГРАММА ОЧИЩЕНИЯ ОРГАНИЗМА В «KAMKOR»</div>
		<div class="programm__items">
			<div class="programm__line programm__line_dark"></div>
			<div class="programm__item">
				<div class="programm__item-header programm__item-header_consult">КОНСУЛЬТАЦИЯ</div>
				<div class="programm__item-desc">Опытный, квалифицированный врач-гельминтолог проконсультирует, поставит диагноз и назначит  необходимую программу лечения. </div>
			</div>
			<div class="programm__line programm__line_light"></div>
			<div class="programm__item">
				<div class="programm__item-header programm__item-header_clear">ОЧИЩЕНИЕ</div>
				<div class="programm__item-desc">Процедура очищения кишечника (очистительные клизмы) проходит с применением специального лекарственного раствора. Все манипуляции проводятся с использованием только одноразовых предметов медицинского назначения. На протяжении всего курса лечения вы получаете лечебный фитококтейль. Сочетание растительных и лекарственных компонентов в этом коктейле позволяет успешно лечить одновременно несколько видов глистных инвазий. Эффективность этого коктейля доказана многолетней практикой использования.</div>
			</div>
			<div class="programm__line programm__line_dark"></div>
			<div class="programm__item">
				<div class="programm__item-header programm__item-header_diagnostic">ДИАГНОСТИКА</div>
				<div class="programm__item-desc">На 2-ой день очистительных процедур берётся анализ с промывных вод, который со 100% точностью позволяет диагностировать наличие гельминтов. Электронный микроскоп экспертного класса с демонстрационным экраном позволяет пациентам самим принимать участие в диагностике и даже идентифицировать гельминты по таблице-классификатору. Всё наглядно и достоверно!</div>
			</div>
			<div class="programm__line programm__line_light"></div>
			<div class="programm__item">
				<div class="programm__item-header programm__item-header_repair">ВОССТАНОВЛЕНИЕ</div>
				<div class="programm__item-desc">После прохождения программы врач-гельминтолог выдает лекарственный растительный сбор и назначает препараты для восстановления микрофлоры кишечника.</div>
			</div>
			<div class="programm__line programm__line_dark"></div>
		</div>
		<div class="programm__cost">
			<div class="programm__cost-wallet"></div>
			<div class="programm__cost-header">СТОИМОСТЬ КУРСА ЛЕЧЕНИЯ</div>
			<div class="programm__cost-cost">от 12 000 тг</div>
		</div>
	</section>
	<section class="unik" id="about">
		<div class="unik__item">
			<div class="unik__item-image unik__item-image_f"></div>
			<div class="unik__item-content">
				<div>
					<div class="unik__header">НАША УНИКАЛЬНОСТЬ</div>
					<div class="unik__text">Медицинский центр «KAMKOR» — единственный в Шымкенте специализированный гельминтологический центр.</div>
				</div>
			</div>
		</div>
		<div class="unik__item">
			<div class="unik__item-content unik__item-content_s">
				<div>
					<div class="unik__item-content-item">Уникальная методика лечения и специализированное диагностическое оборудование.</div>
					<div class="unik__item-content-item">В клинике установлена профессиональная система вентиляции и кондиционирования помещений.</div>
				</div>
			</div>
			<div class="unik__item-image unik__item-image_s"></div>
		</div>
		<div class="unik__item">
			<div class="unik__item-image unik__item-image_t"></div>
			<div class="unik__item-content">
				<div>
					<div class="unik__item-content-item">Все процедуры проводятся в помещениях с отличными санитарно-гигиеническими условиями.</div>
					<div class="unik__item-content-item">Используются только одноразовые материалы и предметы медицинского назначения.</div>
				</div>
			</div>
		</div>
	</section>
	<div class="video">
		<div class="video__inner">
			<div class="video__player">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/2R3fQkmGuHc" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
	<section class="social">
		<div class="social__text">Реальные комментарии, отзывы клиентов, фотографии, полезные статьи смотрите в наших социальных сетях</div>
		<div class="social__links">
			<a href="https://www.instagram.com/kamkor.med/?hl=ru" class="social__link social__link_inst"></a>
			<a href="https://www.facebook.com/kamkor.med/" class="social__link social__link_fb"></a>
			<a href="https://vk.com/kamkormedcenter" class="social__link social__link_vk"></a>
		</div>
	</section>
	<footer class="footer" id="contacts">
		<div class="footer__inner">
			<div class="footer__contacts">
				<div class="footer__contacts-header">КОНТАКТЫ</div>
				<div class="footer__contacts-address">
					Республика Казахстан, <br>
					г. Шымкент, мкр. Нурсат, 124/1 <br>
					(напротив Областной детской больницы)
				</div>
				<div class="footer__contacts-phones">
					<a class="footer__contacts-phone" href="tel:87252771032">8 (7252) 77 10 32</a>
					<a class="footer__contacts-phone" href="tel:+77763208601">+7 776 320 86 01</a>
				</div>
			</div>
			<div class="footer__map" id="map"></div>
		</div>
	</footer>
	<div class="copy">
		<div class="copy__inner">&copy; 2017, ТОО «Мастер Фарм»</div>
	</div>
	<script src="js/main.js"></script>
</body>
</html>