;(function() {
	var links = document.querySelectorAll('.header__nav-a');
	var ymapi 	= document.createElement('script');

	for (var i = 0; i < links.length; i++) {
		links[i].addEventListener('click', function(e) {
			var id = e.target.getAttribute('href').substr(1);
			var block = document.getElementById(id);
			var headerHeight = document.querySelector('.header').clientHeight;

			window.scroll({
				top: block.offsetTop - headerHeight,
				left: 0,
				behavior: 'smooth'
			});

			e.preventDefault();
		});
	}

	ymapi.src 	= 'https://api-maps.yandex.ru/2.1/?lang=ru_RU';
	ymapi.onload 	= function() {
		var map = document.getElementById('map');

		if (map) {
			var myMap;
			ymaps.ready(function() {
				myMap = new ymaps.Map(map, {
					center: [42.358208, 69.624010],
					zoom: 17,
					controls: []
				});
				var template = ymaps.templateLayoutFactory.createClass('<div class="map__mark"></div>');
				var placeMark = new ymaps.Placemark(
						myMap.getCenter(), {}, {
							iconLayout: template
						}
					);
				myMap.geoObjects.add(placeMark);
				myMap.behaviors.disable("scrollZoom");
			});
		}
	}

	setTimeout(function() {
		document.head.appendChild(ymapi);
	}, 1000);
})();